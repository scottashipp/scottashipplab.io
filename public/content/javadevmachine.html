<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Scott Shipp : Common Java developer Macbook setup</title>
    <link rel="stylesheet" href="../css/styles.css" />
</head>

<body>
<div class="content">

    <p><a target="_blank" href="index.html">$ cd -</a></p>
	<hr />
    <h3>Common Java developer Macbook setup</h3>
	<p><em>Last edit: 2021-12-16</em></p>

    <p>I'm a Java developer. This is how I set up a new Macbook. It evolves over time and ymmv. My <a target="_blank" href="../disclaimer.html">standard disclaimer</a> applies.</p>
	
	<h4>Contents</h4>
	<ul>
		<li><a href="#iterm">Install iTerm2</a></li>
		<li><a href="#oh-my-zsh">Install Oh My Zsh!</a></li>
        <li><a href="#install-homebrew">Install Homebrew</a></li>
        <li><a href="#install-git">Install git</a></li>
        <li><a href="#keepassxc">Install KeePassXC</a></li>
        <li><a href="#ssh-key-pair">SSH key pair</a></li>
        <li><a href="#pgp-key-pair">PGP key pair</a></li>
		<li><a href="#setup-for-signing-commits">Setup for signing commits</a></li>
		<li><a href="#setup-development-folder">Setup development folder</a></li>
		<li><a href="#install-java-tools">Install Java tools</a></li>
		<li><a href="#dotfiles">Dotfiles</a></li>
		<li><a href="#checkout-and-build-code">Checkout and build code</a></li>
		<li><a href="#take-a-break">Take a break</a></li>
		<li><a href="#fix-broken-stuff">Fix broken stuff</a></li>
		<li><a href="#be-happy">Be happy</a></li>
		<li><a href="#install-helpful-tools">Install helpful tools</a></li>
		<li><a href="#done">Done?</a></li>
	</ul>
	
	<h4 id="iterm">Install iTerm2</h4>
    <p>I usually start by installing <a target="_blank" href="https://iterm2.com/">iTerm2</a>. I don't think I could handle using the built-in "Terminal" application.
     After opening iTerm, an important thing you should remember to do is <a target="_blank" href="https://iterm2.com/documentation-shell-integration.html">enable shell integration</a>.</p>

    <h4 id="oh-my-zsh">Oh My Zsh!</h4>

    <p>After that, I want <a target="_blank" href="https://ohmyz.sh/">Oh My Zsh!</a> installed. (See the bottom of <a target="_blank" href="https://ohmyz.sh/">their page</a> to find
        out how.) This is a vital necessity.</p>

    <h4 id="install-homebrew">Install Homebrew</h4>
    <p>The next thing I am going to need to do is to install Homebrew. Homebrew allows me to install a number of common software programs with much
    lower effort.</p>
    <p><a target="_blank" href="https://docs.brew.sh/Installation">See the instructions for installing Homebrew</a> and install it accordingly.</p>

    <h4 id="install-git">Install git</h4>
    <p>Once Homebrew is installed, I can use brew to install git:
    <pre>
    $ brew install git
    </pre>
    This also lets me easily get upgrades later.</p>
    <p>If you prefer, though, you can install git <a target="_blank" href="https://git-scm.com/downloads">the hard way</a>.</p>

    <p>Note that use of git to access your repositories requires the configuration of SSH and PGP keys, which is covered below.</p>

    <h4 id="keepassxc">KeePassXC</h4>

    <p>Before going any further, I will need access to my accounts. And that means restoring my KeePassXC database and key file to the appropriate
    places on this new machine. I will not go into details here. The KeePassXC documentation covers every available scenario.</p>

    <p>I install KeePassXC with Homebrew:

    <pre>
    $ brew install --cask keepassxc
    </pre>
    </p>

    <h4 id="ssh-key-pair">SSH key pair</h4>
    <p>One of the first things I need to do is create a new SSH key pair for this device. I do this on each new device, and I do not reuse any SSH key
        pair across devices. If the device ever gets lost or stolen, then I don't have to worry about it. If that ever happened, I would simply
        disable that particular key pair from having access to the various git (or other) platforms that I've allowed it on, and be done with it.</p>

    <p>You may take a different approach and that is fine. It is a tradeoff between either replacing the key on every device you use or replacing the
        key on every platform you use. For me, replacing the key on platforms like GitHub and GitLab is easier than going to each of my devices in
        turn and replacing the key there, so I choose this way.
    </p>
    <p>I set up the key pair according to the <a target="_blank" href="https://dev.to/scottshipp/a-good-way-to-set-up-ssh-keys-for-git-on-macbooks-56l4">A good way to
        set up SSH keys for Git on Macbooks</a> article. When I have time, I'll copy that content here instead.</p>
    <p>Once I have my SSH key pair created, I also supply the public key to my GitHub.com and GitLab.com accounts according to their respective
    documentation. Don't forget to update the $HOME/.ssh/config with the correct configuration, and save the key pairs into KeePassXC.</p>

    <h4 id="pgp-key-pair">PGP key pair</h4>
    <p>Next, I import PGP key pairs for whatever email address(es) I need to use with these different Git platforms (or otherwise).</p>
    <p>My general approach for securely transferring PGP keys from machine to machine is with ssh. You can log into the machine with the
        PGP key pair you want and <em>push</em> it to your new machine with:</p>

    <pre>
    $ gpg --export-secret-key SOMEKEYID | ssh othermachine gpg --import
    </pre>

    <p>Or you can log into the new machine and <em>pull</em> the PGP key pair in question with:</p>

    <pre>
    $ ssh othermachine gpg --export-secret-key SOMEKEYID | gpg --import
    </pre>

    <h4 id="setup-for-signing-commits">Setup for signing commits</h4>
    <p>Next, I set up git accounts with the appropriate keys so that I always use signed commits. (If you don't do this, why don't you? As a
        developer, I can't imagine a good reason. It feels irresponsible to me to not sign commits.)</p>

    <p>Here are the articles for three popular git platforms that have to do with signing commits:</p>
    <ul>
        <li><a target="_blank" href="https://docs.github.com/en/authentication/managing-commit-signature-verification/signing-commits">Signing commits</a> in GitHub Docs</li>
        <li><a target="_blank" href="https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/">Signing commits with GPG</a> in GitLab Docs</li>
        <li><a target="_blank" href="https://confluence.atlassian.com/bitbucketserver/using-gpg-keys-913477014.html">Using GPG keys</a> in BitBucket Support</li>
    </ul>

    <p>Generally the above instructions include configuring your local git with the right signing key, but I want to be explicit on this point. You
        MUST execute a command like this in order to configure git to use your PGP key:</p>

    <pre>
    $ git config --global user.signingkey SOMEKEYID
    </pre>

    <p>Furthermore, the above signing key MUST match the email configured in the user.email parameter. Once you <em>think</em> you have configured
        everything correctly, you will probably want to just double-check that both are set, and that they are set to the correct values. You can do
        that with:</p>

    <pre>
    $ git config user.email && git config user.signingkey
      scott.shipp@engineer.com
      632B0E75B37B5132
    </pre>

    <p>Finally, if you actually have multiple Git repositories that are all signed with different keys, then you'll have to figure out which one you use the
        most often, and make it the global value, with:

    <pre>
    $ git config --global user.signingkey SOME_KEY_ID
    $ git config --global user.email THE_MATCHING_EMAIL
    </pre>

    <p>Then, you also need to go to each disparate repository, and configure their specific email and signing key. The commands for this are the same
        as above except they omit the --global flag.</p>

    <p>If you don't understand git configuration, read the <a href="https://www.git-scm.com/book/en/v2/Customizing-Git-Git-Configuration">relevant
        section in Pro Git</a>.</p>

    <h4 id="setup-development-folder">Setup Development folder</h4>
    <p>My preference is to create a new folder, Development, stored at $HOME/Documents/Development, where all source control repositories are
        checked out and organized into a hierarchical fashion.</p>

    <p>This usually means the following sequence of commands on a modern Macbook with zsh (zsh replaced bash when macOS Catalina came out):</p>
    <pre>
    $ cd $HOME
    $ mkdir -p $HOME/Documents/Development
    $ ln -s $HOME/Documents/Development $HOME/dev
    $ cd dev
    $ mkdir github.com
    $ mkdir gitlab.com
    $ mkdir {{URL of private Git repository}}
    $ mkdir {{you probably get the picture...a directory for each Git platform I use}}
    </pre>

    <h4 id="install-java-tools">Install Java Tools</h4>

    <p>The next step, of course, is to get some Java tooling installed. At a bare minimum, this means the following.</p>

    <p>I install <a target="_blank" href="https://sdkman.io/install">SDKMan!</a>, and then use it to install and set an appropriate default JDK.</p>

    <p>I install <a target="_blank" href="https://maven.apache.org/">Apache Maven</a>.</p>

    <p>I may also install <a target="_blank" href="https://gradle.org/">gradle</a> if I have any applications using it.</p>

    <p>I install the <a target="_blank" href="https://www.jetbrains.com/toolbox-app/">JetBrains Toolbox</a>, and then I use it to install an appropriate version of
        <a target="_blank" href="https://www.jetbrains.com/idea/">IntelliJ IDEA</a>. I also
        <a target="_blank" href="https://www.jetbrains.com/help/idea/working-with-the-ide-features-from-command-line.html#toolbox">use Toolbox to install the idea cli</a>.
    </p>

    <p>There are probably others I am forgetting, but which I will be adding here in the future.</p>

    <h4 id="dotfiles">Dotfiles</h4>
    <p>Finally! I can now checkout my dotfiles. I created my dotfiles repository according to the popular streakycobra method. This is described
    better by <a target="_blank" href="https://www.atlassian.com/git/tutorials/dotfiles">an Atlassian tutorial</a> so if you don't have dotfiles that's the best place
        to go to learn how to set up dotfiles.</p>

    <p>The dotfiles are a bit of a chicken-and-egg proposition because as you have observed so far, I had to get git installed <em>and configured</em>
        up to this point, but I'm about to check out my dotfiles and overwrite that configuration. So be wise and back up your .gitconfig file first.
        The same applies to anything else you may have heretofore configured.
    </p>

    <p>Anyway, I can now navigate to my dev folder, clone my dotfiles repo locally, and then set them up.</p>

    <h4 id="checkout-and-build-code">Checkout and build code</h4>
    <p>At this point, I will probably clone a bunch of repos and start building them with Maven. This will cause a massive dump of artifacts to my
    local Maven cache, and that is the point. I know that this step will take awhile, and so I've ordered everything above to get me here as quick
    as possible.</p>

    <p>Once everything is building, now is a good time for a break.</p>

    <h4 id="take-a-break">Take a break</h4>
    <p>Coffee?</p>

    <h4 id="fix-broken-stuff">Fix broken stuff</h4>
    <p>I will check back periodically, and probably I will find a bunch of issues I need to fix.</p>

    <h4 id="be-happy">Be happy</h4>
    <p>When stuff finally builds, it is really a great feeling! But we're not done yet.</p>

    <h4 id="install-helpful-tools">Install helpful tools</h4>
    <p>Every mac user probably uses <a target="_blank" href="https://rectangleapp.com/">Rectangle</a> because it is a superior window manager. I install it.</p>

    <p>Next comes a better lightweight plain-text editor: <a target="_blank" href="https://macromates.com/">TextMate</a>. In fact, until I install TextMate, my
    ability to make commits to git is probably broken, since I configure git to use it for all commit messages.</p>

    <p>Speaking of git, there are a couple really useful shell utilities that help with git's output <em>a lot.</em> And I mean A LOT! These are
    <a target="_blank" href="https://github.com/sharkdp/bat">bat</a> and <a target="_blank" href="https://github.com/dandavison/delta">delta</a>. Special thanks to Aaron Davis for
        tipping me off on these. You don't even have to be a <em>Java</em> developer to appreciate bat and delta.</p>

    <p>Next, I probably install and set up my own <a target="_blank" href="https://github.com/scottashipp/noted/">noted cli</a> for taking notes.</p>

    <p>I also install this <a target="_blank" href="software-quotes.dat">software-quotes.dat</a> fortune file, because it's fun to see a random quote like the below
        whenever I open a new iTerm window/pane.

        <pre>
    Let us change our traditional attitude to the construction of programs. Instead
    of imagining that our main task is to instruct a computer what to to, let us
    concentrate rather on explaining to human beings what we want a computer to
    do.

    Donald Knuth
    </pre></p>

    <p>That reminds me, I install <a target="_blank" href="https://github.com/busyloop/lolcat">lolcat</a> so I can see these quotes in a rainbow of colors.

        <pre>
    $ brew install lolcat
</pre>
    To make sure that's in every new iTerm window or pane, I add this to my .zshrc file:
    <pre>
    fortune software-quotes | lolcat
    </pre>
    </p>

    <h4 id="done">Done?</h4>
    <p>I'll keep updating this with more but I think that covers <em>most</em> of the basics anyway. You may imagine that some or all of Slack,
    Postman, node, npm, Zoom, etc. end up on there too.</p>
	
	<p>&mdash;\x53.\x41.\x53

	<hr />
	<p><a target="_blank" href="index.html">$ cd -</a></p>
    </p>
</div>

</body>
</html>

