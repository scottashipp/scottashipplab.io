<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Scott Shipp : Thoughts on Privacy & Security</title>
    <link rel="stylesheet" href="../css/styles.css" />
</head>

<body>
<div class="content">

    <p><a target="_blank" href="index.html">$ cd -</a></p>
    <hr />
    <h2>Thoughts on Online Privacy & Security</h2>
    <p>Last update: 2023-09-19</p>
    <p>I've been a software maker for almost twenty years. Sometimes people ask me how to stay safe online, not get tracked by cookies, and so on.
        Below, I share a jumble of privacy and security skills I have picked up over that time. Some of them are easy to implement, but
        unfortunately rare, like picking long passwords. Others are practiced by those "in the know" but often not shared with the general public.
    </p>

    <p>Well...maybe now you are in the know also.</p>

    <p>There are three main sub-articles:</p>
    <ol>
        <li>Privacy and Security are not the same</li>
        <li>Methods for maintaining privacy online</li>
        <li>Methods for maintaining security online</li>
    </ol>

    <br /><hr /><br />

    <h3>Privacy and Security are not the same</h3>
    <p>The most important point to make up front is that privacy and security <strong>are not the same thing</strong>. The reason they are mentioned together
        so often is that they are <em>related</em>, but that's it. It's important not to think that if something is secure, it is also private,
        or that if something is private, it is also secure. You can have one, the other, or both. This can be hard to grasp because most of the
        obvious things you want to keep private are also things you want to keep secure. Your wallet, your medical history, or illicit photos of your
        significant other are some examples. But there are plenty of cases where things are <em>only</em> private or <em>only secure</em>.
    </p>

    <h4>Some examples</h4>

    <p>For example, someone may not care much about
        the privacy of where they bank or how much money is there. Actually, a lot of people are quite open about things like that. That can be public
        information as long as the bank itself can actually be trusted to hold the money in a vault with guards in front of it. This is security
        without privacy. A related example is a checking account number. It is written on the bottom of every check and freely passed around "in the
        clear." However, withdrawing funds from the same account requires you to have secure access.
    </p>
    <p>The reverse can also be true. Deliveries to your house are often private but not secure. Anyone can take mail or packages from your mailbox or
        front porch (and they often do) but at least the contents of these packages are generally inside an envelope or a box. You probably don't
        send all letters as postcards. You expect some level of privacy, but no level of security.
    </p>
    <p>The same is true online. For example, even if every tweet you write is public, you still expect that access to your account is secured such
        that only you can tweet from it.
    </p>
    <p>So security and privacy are different. If you want something to be both secure and private, you have to both secure that
        something and also privatize that something. But there are cases where you only want one or the other.
    </p>

    <h4>The web is fundamentally open</h4>

    <p>The next important observation is that the world wide web is fundamentally <em>open</em>. It is neither secure nor private. Before you get
    angry about that, just remember it was purposefully designed that way. The original tag line of the world wide web was the "information
    superhighway." The point of the whole exercise was to share as much information as possible, as openly as possible. Early pioneers of the web
    envisioned it as completely shared and editable by all, which is why the first web browser
        (<a href="https://en.wikipedia.org/wiki/WorldWideWeb">WorldWideWeb</a>) was both an editor and viewer. In addition, insecure http was how most
        web sites were served until very recently when browsers enacted policies to force web site owners to buy TLS certificates and serve their
        sites over https. This decision is one of those "bad actors f'ed it up for the rest of us" decisions, because many sites online&mdash;like this
        one, actually&mdash;have absolutely no need for TLS.
    </p>

    <p>The open-by-default nature of the web is often surprising and upsetting to people, even people who surf the web or use a smartphone every day.
        Never fear, though. It <em>is</em> possible to be both private <em>and</em> secure online. There are steps you can take to maintain your
        privacy and security. Below are a few simple methods for enabling privacy and security protections while you engage in what are considered
        "normal" online activities.</p>

    <p>But before we cover those, let's make one more important observation. If you are someone particularly concerned with privacy and security,
        then the world wide web may not be the place where you want to conduct certain activities at all. It can be more secure and private to conduct
        activities offline. By doing so, you will obviate any online privacy and security concerns. Just remember that the world is so connected to
        the web today that even some activities in the real world end up online. Just ask all the people who <a href="https://www.vice.com/en/article/bmyded/the-strange-beauty-of-finding-yourself-in-google-street-view">find themselves in Google Maps' "street
        view"</a> or learn that by purchasing lunch with a credit card they are now regaled with junk email.
    </p>

    <br /><hr /><br />

    <h3>Methods for maintaining privacy online</h3>

    <h4>Use privacy-centered products to access the web</h4>
    <p>Your operating system, web browser, or search engine might all be privacy concerns. Why not try using a <a href="https://en.wikipedia.org/wiki/Live_CD">live CD / USB</a> operating
        system like <a href="https://knoppix.net">Knoppix</a> or <a href="https://tails.boum.org/">Tails</a>?
    </p>
    <p>If you don't feel the need to take such precautions, simply switch your browser to <a href="https://brave.com/">Brave</a> or
        <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> instead? And while you're at it, why not try a search with
        <a href="https://duckduckgo.com">DuckDuckGo</a> instead of that other search engine that rhymes with Ooogle?
    </p>

    <h4>Use a standalone email address</h4>
    <p>Almost all web sites rely on the idea of an "account" which is tied to an email address. Online privacy in 2022 is such a huge concern that
        even large mail providers are introducing email masking or temporary email address features. One longstanding standalone service for temporary
        email addresses is <a href="https://www.guerrillamail.com/">Guerrilla Mail</a>. By using a temporary email address, it possibly cannot be
        traced back to you, and thus helps a given account remain more anonymous.
    </p>
    <p>A similar idea is Apple's more-recently-introduced <a href="https://support.apple.com/en-us/HT210425">Hide my Email</a> product for iCloud
        subscribers.</p>
    <p>It may seem that the point of a standalone or temporary email address is to remain anonymous, but this is usually not the case. You would have
    to be extremely cautious in other ways in order to remain anonymous. The true protection that such an email address affords you is that if you,
    for example, used your personal email (say, "john.doe@gmail.com") for Target.com, and you use it with a particular password (say, "password" for
        the sake of example), then it is likely you re-use this combination everywhere. (Perhaps even for logging into gmail?) So by simply cracking
    Target's database, a bad actor can now log in as you to many places online. One nice thing about one-off / per-account email addresses is that it
    subverts this possibility. But, you should practice the password recommendations given later in this article which are even more helpful toward
    this end.

    <h4>Use plus-addressing (also known as subaddressing)</h4>
    <p>A related idea is <a href="https://en.wikipedia.org/wiki/Email_address#Subaddressing">plus addressing / subaddressing</a> which allows you to
    use a unique email address for each account while still using your primary email. Confused? Let me explain. Generally, this takes the form of your
        email address, the plus symbol, and some specific tag to uniquely identify the email address. For example, let's say your email address is
        mail@example.org. You might create a YouTube account using the email mail+youtube@example.org. Mail sent to this address will still land in
    your inbox, but when you are careful to use such an address for only a single online account, you have many more options for protecting yourself
    in the case of a breach.</p>
    <p>The advantage is that if YouTube is not responsible with your data&mdash;let's say they sell it to spammers&mdash;then you're able
    to easily identify that this has happened and take an action in response. As soon as you start to notice a lot of Nigerian "princes" who need to
    quickly get their money out of the country, and all their emails are addressed to mail+youtube@example.org, you know what has occurred. In
    response, you can make a new email address, perhaps mail+yt@example.org, and change your YouTube account over to it, meanwhile blocking any email
        directed toward mail+youtube@example.org.
    </p>
    <p>One other nice thing about plus-addressing is that many mail providers have started to build useful features around it. For example, <a href="">FastMail</a>
        users will find that each email they receive which is addressed to a plus address will automatically sort itself into a folder, provided that
        the folder has the same name as the "tag" part of the address (the part after the "+" and before the "@").
    </p>

    <h4>Use end-to-end encryption</h4>
    <p>If you have to message with others online, use something like <a href="https://www.signal.org/">Signal</a>, which is encrypted end-to-end.</p>
    <p>Protonmail is an email option. As long as you only email with other Protonmail addresses, the communication is encrypted end-to-end.</p>
    <p>With some effort, you can even learn encryption technologies like <a href="https://www.openpgp.org">PGP</a> to provide over-the-top encryption
        using unsecured channels.</p>

    <h4>Don't enter your real name, phone number, email, or address unless you understand the risks</h4>
    <p>It doesn't matter if you take all of the abovementioned steps and still enter your real name, address, phone number, or email into the sites you
        visit. Companies use that data to connect all your online activity into a profile about you. If you don't believe me, try logging into any of
        your Google accounts and then visit <a href="https://myaccount.google.com/dashboard">this dashboard</a>.
    </p>
    <p>So when it comes time to place an order, conduct a financial transaction, or anything else online, know the terms of service you are agreeing
    to and consider how the other party will sell or use your data. Act accordingly.</p>
    <p>The privacy-minded individual can still engage in online ordering, while protecting their address, for example, if they use a PO box or a
        package-forwarding service.</p>

    <h4>Be careful what search engine you choose</h4>
    <p>Google is the largest advertising machine in recorded history. You could consider using a privacy-minded alternative. A popular one is
    DuckDuckGo.</p>

    <br /><hr /><br />

    <h3>Methods for Addressing online security concerns</h3>

    <p>The following steps should allow you to secure your online accounts.</p>

    <h4>Use a unique password for each account</h4>
    <p>Plan for each account to be compromised. Statistically, it will happen to one or more of your accounts, you just don't know when or which
        account. Therefore, use a unique password for each account so that once someone cracks it, they can't use the same password to then log
        into and take over other accounts. If an account gets compromised, you want the damage to be limited in place.
    </p>

    <p>The only way to make a unique password for each account practical is to use a password manager like <a href="https://keepass.info/">KeePass</a>
        or its variants. Many people are happy with a cloud service like <a href="https://bitwarden.com/">BitWarden</a> instead. LastPass is a popular
        option but it is <a href="https://hn.algolia.com/?q=lastpass">mistrusted by those in the know</a>. Still, if you have to use LastPass, it is
        better than what most people do, which is to use the same easily-guessable password for everything from their bank account to their email to
        their Netflix account. Don't be like them. Be smart, and use a unique, strong, non-guessable password for each account. Read on to find out how.
    </p>

    <h4>Use strong passwords</h4>
    <p>When you generate a unique password, make sure it is also strong. What is a strong password? <a href="https://www.bitdefender.com/blog/hotforsecurity/hackers-dont-bother-trying-to-guess-strong-passwords-new-research-shows/">Research</a> shows that the longer a password is,
        the better. A password doesn't actually derive much extra strength from using additional components like mixed capitalization, numbers, and
        symbols. One of the best ways you can generate strong passwords is through the <a href="https://secure.research.vt.edu/diceware/#eff">diceware</a> technique.</p>

    <p>But don't forget to take the step of a unique password for each account also.</p>

    <h4>Split up online accounts among email accounts</h4>

    <p>Let's say you are using a unique email account for each online account, through the use of "plus-addressing" as mentioned above. You still may
    want to consider obtaining some additional email addresses and splitting up your online accounts among them. This would be a security step geared
    towards the possibility of the email address itself getting compromised. If someone gains access to your email, they can reset all passwords of
    all accounts connected to it and gain control over things like your bank accounts and credit cards. Why put all your eggs in one basket?</p>

    <p>So as an added step to the above, you can consider taking the step of separating accounts among multiple email addresses. This is not true
        security, but it could help to limit the damage of someone compromising a single one of those email accounts, as they can then only use that
        email address to gain control of the now-limited number of accounts connected to it.</p>

    <p>I consider large public sites like Reddit and YouTube their own specific threat. One thing to consider about data breaches is that they usually
        include both the email account and the password being used. That email account / password combination then gets resold on the dark web, and
        can then become a target of crackers who crack that email account and can gain access to data they can use for identity theft or
        for cracking further accounts. It may therefore be wise to use an email account on potentially-breachable sites like YouTube or Reddit that
        is unconnected to any further data. It may be wise, for example, to never use the email address you use for all financial-related online
        accounts (like your bank, credit card, loans, budgeting software, etc.) for one of these other sites.
    </p>

    <h4>Should you use a VPN?</h4>

    <p>Recently it has become popular to use a VPN service like the heavily-marketed NordVPN. (I am purposefully not linking to them nor endorsing
    them.) The major problem with using a VPN is that although it is true that a VPN tunnels your online traffic across the web, making it impossible
    for others to observe it <em>while in transit</em>, most people still freely share a lot of personal information this way, and this at least partly
    neutralizes any benefit of a VPN. A VPN can be "an extra measure of privacy" for the privacy-minded individual, but not for the averag person who
    continues to volunteer their data all over the web, albeit across VPN.</p>

    <p>I personally do not use a VPN for this reason. I instead practice at least all of the other things listed above here. But your mileage may
    vary.</p>

    <h4>Should you use Tor?</h4>

    <p>The same comments I made above for VPN apply to Tor. There's no point in using it if you are still going to behave like most web surfers.</p>

    <h4>Should you enable the location (GPS) feature on your smartphone?</h4>
    <p>Location sharing via GPS tracking of your smartphone has become popular and is used for all kinds of valid use cases, such as letting family
    members know where you are or for recording the route you run or cycle. But it also has some danger. For example, it was recently discovered that
    the popular cycling app "Strava," which is used by cyclists to record the fastest times on known public routes, in a sort of ongoing public
    competition, was also being used by bike thieves. They would simply look up the start and end point of the most common rides by the fastest
    riders, and (correctly) assume that they must live somewhere nearby. They would then show up to such locations, quietly follow that person home
    and mark where they live. Later, they would break in and take their expensive $10,000+ bikes.</p>

    <p>Location sharing has its downsides and this is one of them. In the early days of Twitter, people found that if they would tweet pictures of
    themselves while on vacation, thieves would note that they were away and go break into their house. The same principle applies. So be very
    careful how you use location on your phone. The way I use it is to keep it off, use it for finding directions to places I don't know how to
    drive to, and then turn it off again right afterward.</p>

    <h4 />

    <h3>Closing Remarks</h3>

    <p>Above are some ideas for beginning to think about protecting your online privacy and securing your financial and other assets. If you are
    interested in this topic, check out some books like "The Age of Surveillance Capitalism" by Shoshana Zuboff. The Electronic Frontier Foundation is
    a non-profit that publishes some interesting and helpful resources also.  Even the UCLA has some helpful things to say about online rights like
    privacy rights upon occasion. Use these resources and good luck!

    <hr />

    <p><a target="_blank" href="index.html">$ cd -</a></p>
    </p>
</div>

</body>
</html>

